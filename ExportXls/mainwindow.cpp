#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tppicinfo.h"

#include <QTableView>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QTableWidget>
#include <QString>
#include <QCheckBox>
#include <QTextCodec>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include <QDebug>
#include <QFileDialog>
#include <QAxObject>
#include <qobject.h>
#include <QLabel>
#include <QTextEdit>
#include <QMessageBox>
#include <QByteArray>
#include <QPixmap>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //确定编码格式避免中文乱码
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

    cb1.setText("1 弧度");
    cb2.setText("2 周长");
    cb3.setText("3 高度");
    cb4.setText("4 面积");
    cb5.setText("5 距离");
    cb6.setText("6 角度");
    cb7.setText("7 宽度");
    cb8.setText("8 半径");

    QGridLayout *glayout = new QGridLayout;
    glayout->addWidget(&cb1,0,0);
    glayout->addWidget(&cb2,0,1);
    glayout->addWidget(&cb3,0,2);
    glayout->addWidget(&cb4,1,0);
    glayout->addWidget(&cb5,1,1);
    glayout->addWidget(&cb6,1,2);
    glayout->addWidget(&cb7,2,0);
    glayout->addWidget(&cb8,2,1);

    QPushButton *btn = new QPushButton;
    btn->setText("转换表格");

    glayout->addWidget(btn,3,1);
    QWidget *mainWidget = new QWidget;
    mainWidget->setLayout(glayout);
    setCentralWidget(mainWidget);
    resize(300,150);

    tppicinfo *tpinfo = new tppicinfo("1","2","3","4","5","6","7","8","9");
    tppicinfo *tpinfo1 = new tppicinfo("名称","湖","周长","搞","mian","Ju","绞","髋","半径");
    vector.append(*tpinfo);
    vector.append(*tpinfo1);

    connect(btn,SIGNAL(clicked()),this,SLOT(showTable()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showTable(){
    t->setObjectName("tableWidget");
    t->setEditTriggers(QAbstractItemView::NoEditTriggers);
    t->setRowCount(vector.size());
    t->setColumnCount(9);
    QTableWidgetItem *tw = new QTableWidgetItem(QString("名称"));
    t->setHorizontalHeaderItem(0,tw);
    int row=0;
    int col=1;
    QVector<tppicinfo>::const_iterator vptr = vector.constBegin();
    for (; vptr != vector.constEnd(); vptr++) {
        tw = new QTableWidgetItem(vptr->getName());
        t->setItem(row++,0,tw);
    }
    row=0;
    if(cb1.checkState()){
        vptr = vector.constBegin();
        for (;vptr != vector.constEnd();vptr++){
            tw = new QTableWidgetItem(vptr->getRadian());
            t->setItem(row++,col,tw);
        }
        row=0;
        tw = new QTableWidgetItem(QString("弧度"));
        t->setHorizontalHeaderItem(col++,tw);
    }
    if(cb2.checkState()){
        vptr = vector.constBegin();
        for (;vptr != vector.constEnd();vptr++){
            tw = new QTableWidgetItem(vptr->getPerimeter());
            t->setItem(row++,col,tw);
        }
        row=0;
        tw = new QTableWidgetItem(QString("周长"));
        t->setHorizontalHeaderItem(col++,tw);
    }
    if(cb3.checkState()){
        vptr = vector.constBegin();
        for (;vptr != vector.constEnd();vptr++){
            tw = new QTableWidgetItem(vptr->getHeight());
            t->setItem(row++,col,tw);
        }
        row=0;
        tw = new QTableWidgetItem(QString("高度"));
        t->setHorizontalHeaderItem(col++,tw);
    }
    if(cb4.checkState()){
        vptr = vector.constBegin();
        for (;vptr != vector.constEnd();vptr++){
            tw = new QTableWidgetItem(vptr->getArea());
            t->setItem(row++,col,tw);
        }
        row=0;
        tw = new QTableWidgetItem(QString("面积"));
        t->setHorizontalHeaderItem(col++,tw);
    }
    if(cb5.checkState()){
        vptr = vector.constBegin();
        for (;vptr != vector.constEnd();vptr++){
            tw = new QTableWidgetItem(vptr->getDistance());
            t->setItem(row++,col,tw);
        }
        row=0;
        tw = new QTableWidgetItem(QString("距离"));
        t->setHorizontalHeaderItem(col++,tw);
    }
    if(cb6.checkState()){
        vptr = vector.constBegin();
        for (;vptr != vector.constEnd();vptr++){
            tw = new QTableWidgetItem(vptr->getAngle());
            t->setItem(row++,col,tw);
        }
        row=0;
        tw = new QTableWidgetItem(QString("角度"));
        t->setHorizontalHeaderItem(col++,tw);
    }
    if(cb7.checkState()){

        vptr = vector.constBegin();
        for (;vptr != vector.constEnd();vptr++){
            tw = new QTableWidgetItem(vptr->getWidth());
            t->setItem(row++,col,tw);
        }
        row=0;
        tw = new QTableWidgetItem(QString("宽度"));
        t->setHorizontalHeaderItem(col++,tw);
    }
    if(cb8.checkState()){
        vptr = vector.constBegin();
        for (;vptr != vector.constEnd();vptr++){
            tw = new QTableWidgetItem(vptr->getRadius());
            t->setItem(row++,col,tw);
        }
        row=0;
        tw = new QTableWidgetItem(QString("半径"));
        t->setHorizontalHeaderItem(col++,tw);
    }
    row=vector.size();
    t->setColumnCount(col);
    t->horizontalHeader()->setDefaultSectionSize(100);
    t->verticalHeader()->setDefaultSectionSize(40);
//    t->resize(col*100+70,row*40+2);
    QVBoxLayout *layout =new QVBoxLayout;
    QPushButton *btn = new QPushButton;
    btn->setText("导出表格");
    layout->addWidget(btn);
    QVBoxLayout *mainlayout = new QVBoxLayout;
    mainlayout->addWidget(t);
    mainlayout->addLayout(layout);
    QWidget *main = new QWidget(this);
    main->setLayout(mainlayout);
    main->resize(col*100+50,row*40+82);

    connect(btn,SIGNAL(clicked()),this,SLOT(exportTable()));
    main->setWindowModality(Qt::WindowModal);
    main->setWindowFlags(Qt::Dialog);
    main->show();

}

void MainWindow::exportTable(){
    QString *filepath = new QString("D://1.xls");
    QString picpath = "C:\\Users\\touptek\\Pictures\\Saved Pictures\\IMG_0001.jpg";
    QString newpath ="C:\\Users\\touptek\\Pictures\\Saved Pictures\\IMG_0002.jpg";

    QPixmap * pix = new QPixmap(picpath);
    *pix = pix->scaled(960,540,Qt::KeepAspectRatio);
    pix->save(newpath,"jpg");

    QFile file(*filepath);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QMessageBox::warning(this,QString("错误"),QString("导出发生错误！"));
        qDebug()<<"发生错误无法写入";
        return;
    }

    QString html;

    html+="<html xmlns:v=\"urn:schemas-microsoft-com:vml\"\n"
          "xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n"
          "xmlns:x=\"urn:schemas-microsoft-com:office:excel\">\n"
          "<head>\n"
          "</head>\n"
          "<body>\n"
          "\t<table x:str style='border-collapse:collapse;table-layout:fixed;width:960pt'>\n"
          "\t\t<tr height=540 style='mso-height-source:userset;height:540pt'>\n"
          "\t\t\t<td colspan=9 height=2160 width=3840 style='height:540pt;width:960pt'>\n"
          "\t\t\t\t<span>\n"
          "\t\t\t\t\t<table>\n"
          "\t\t\t\t\t\t<tr>\n"
          "\t\t\t\t\t\t\t<td colspan=9 style='height:540pt; width:960pt'>\n"
          "\t\t\t\t\t\t\t<img src=\""+newpath+"\">\n"
          "\t\t\t\t\t\t\t</td>\n"
          "\t\t\t\t\t\t</tr>\n"
          "\t\t\t\t\t</table>\n"
          "\t\t\t\t</span>\n"
          "\t\t\t</td>\n"
          "\t\t</tr>\n\n"
          "\t\t<tr>\n";


    for(int i=0;i<t->columnCount();i++){
        html+="\t\t\t<td rowspan=1 style='border-bottom:.5pt solid black;border-top:.5pt solid black;border-right:.5pt solid black;'>"+t->horizontalHeaderItem(i)->text()+"</td>\n";
    }
    html+="\t\t</tr>\n";

    for(int i=0;i<t->rowCount();i++){
        html+="\t\t<tr>\n";
        for (int j=0;j<t->columnCount();j++){
           html+="\t\t\t<td rowspan=1 style='border-bottom:.5pt solid black;border-right:.5pt solid black;'>"+t->item(i,j)->text()+"</td>\n";
        }
        html+="\t\t</tr>\n";
    }
    html+="\t</table>\n";
    html+="</body>";
    file.write(QByteArray(html.toStdString().c_str()));
    file.close();
    qDebug()<<"导出完成！";
}


//void MainWindow::exportTable(){
//    //获取保存路径
//    QString filepath=QFileDialog::getSaveFileName(this,tr("Save"),".",tr(" (*.xls)"));
//    if(!filepath.isEmpty()){
//        QAxObject *excel = new QAxObject();
//        excel->setControl("Excel.Application");
//        excel->dynamicCall("SetVisible (bool Visible)","false");
//        excel->setProperty("DisplayAlerts", false);
//        QAxObject *workbooks = excel->querySubObject("WorkBooks");
//        workbooks->dynamicCall("Add");
//        QAxObject *workbook = excel->querySubObject("ActiveWorkBook");
//        QAxObject *worksheets = workbook->querySubObject("Sheets");
//        QAxObject *worksheet = worksheets->querySubObject("Item(int)",1);

//        QTableWidget *tableWidget = this->t;

//        //设置表头值
//        for(int i=1;i<tableWidget->columnCount()+1;i++)
//        {
//             //设置设置某行某列
//             QAxObject *Range = worksheet->querySubObject("Cells(int,int)", 1, i);
//             Range ->setProperty("HorizontalAlignment", -4108);
//             Range ->setProperty("VerticalAlignment", -4108);
//             Range->dynamicCall("SetValue(const QString &)",tableWidget->horizontalHeaderItem(i-1)->text());
//         }
//        //设置表格数据
//        for(int i = 1;i<tableWidget->rowCount()+1;i++)
//        {
//            for(int j = 1;j<tableWidget->columnCount()+1;j++)
//            {
//                QAxObject *Range = worksheet->querySubObject("Cells(int,int)", i+1, j);
//                Range ->setProperty("HorizontalAlignment", -4108);
//                Range ->setProperty("VerticalAlignment", -4108);
//                Range->dynamicCall("SetValue(const QString &)",tableWidget->item(i-1,j-1)->data(Qt::DisplayRole).toString());
//            }
//        }
//        workbook->dynamicCall("SaveAs(const QString&)",QDir::toNativeSeparators(filepath));//保存至filepath
//        workbook->dynamicCall("Close()");//关闭工作簿
//        excel->dynamicCall("Quit()");//关闭excel
//        delete excel;
//        excel=NULL;
//        qDebug() << "导出成功！";
//    }
//}

//void MainWindow::exportTable(){
//
//    QString *filepath = new QString("D://1.xls");
//    QFile file(*filepath);

//    if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
//    {
//        QMessageBox::warning(this,QString("错误"),QString("导出发生错误！"));
//        qDebug()<<"发生错误无法写入";
//        return;
//    }

//    QString xml;
//    xml+="<?xml version=\"1.0\"?>\n";
//    xml+="<ss:Workbook xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\">\n";
//    xml+="\t<ss:Worksheet ss:Name=\"Sheet1\">\n";
//    xml+="\t\t<ss:Table>\n";

//    //标题行
//    xml+="\t\t\t<ss:Row>\n";
//    for(int i=0;i<t->columnCount();i++){
//        xml+="\t\t\t\t<ss:Cell>\n";
//        xml+="\t\t\t\t\t<ss:Data ss:Type=\"String\">"+t->horizontalHeaderItem(i)->text()+"</ss:Data>\n";
//        xml+="\t\t\t\t</ss:Cell>\n";
//    }
//    xml+="\t\t\t</ss:Row>\n";

//    //内容行
//    for(int i=0;i<t->rowCount();i++){
//        xml+="\t\t\t<ss:Row>\n";
//        for(int j=0;j<t->columnCount();j++){
//            xml+="\t\t\t\t<ss:Cell>\n";
//            xml+="\t\t\t\t\t<ss:Data ss:Type=\"String\">"+t->item(i,j)->text()+"</ss:Data>\n";
//            xml+="\t\t\t\t</ss:Cell>\n";
//        }
//        xml+="\t\t\t</ss:Row>\n";
//    }


//    xml+="\t\t</ss:Table>\n";
//    xml+="\t</ss:Worksheet>\n";
//    xml+="</ss:Workbook>\n";



//    file.write(QByteArray(xml.toStdString().c_str()));
//    file.close();
//    qDebug()<<"导出完成";

//}

