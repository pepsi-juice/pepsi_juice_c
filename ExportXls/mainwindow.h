#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCheckBox>
#include "tppicinfo.h"
#include <QVector>
#include <QTableWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = NULL);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QCheckBox cb1;
    QCheckBox cb2;
    QCheckBox cb3;
    QCheckBox cb4;
    QCheckBox cb5;
    QCheckBox cb6;
    QCheckBox cb7;
    QCheckBox cb8;
    QVector<tppicinfo> vector;
    QTableWidget *t = new QTableWidget(9,9);

public slots:
    void showTable();
    void exportTable();
};
#endif // MAINWINDOW_H
