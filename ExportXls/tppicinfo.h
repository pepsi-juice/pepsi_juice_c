#ifndef TPPICINFO_H
#define TPPICINFO_H

#include <QString>
class tppicinfo{
private:
    QString m_name;
    QString m_radian;
    QString m_perimeter;
    QString m_height;
    QString m_area;
    QString m_distance;
    QString m_angle;
    QString m_width;
    QString m_radius;

public:
    tppicinfo(QString,QString,QString,QString,QString,QString,QString,QString,QString);
    tppicinfo();
    ~tppicinfo();

    QString getname();
    QString getradian();
    QString getperimeter();
    QString getheight();
    QString getarea();
    QString getdistance();
    QString getangle();
    QString getwidth();
    QString getradius();
    void setname(QString);
    void setradian(QString);
    void setperimeter(QString);
    void setheight(QString);
    void setarea(QString);
    void setdistance(QString);
    void setangle(QString);
    void setwidth(QString);
    void setradius(QString);
\
    QString getPerimeter() const;
    void setPerimeter(const QString &perimeter);
    QString getHeight() const;
    void setHeight(const QString &height);
    QString getName() const;
    void setName(const QString &name);
    QString getRadian() const;
    void setRadian(const QString &radian);
    QString getWidth() const;
    void setWidth(const QString &width);
    QString getRadius() const;
    void setRadius(const QString &radius);
    QString getAngle() const;
    void setAngle(const QString &angle);
    QString getDistance() const;
    void setDistance(const QString &distance);
    QString getArea() const;
    void setArea(const QString &area);
};

#endif // TPPICINFO_H
