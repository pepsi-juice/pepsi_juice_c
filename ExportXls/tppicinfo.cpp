#include "tppicinfo.h"



tppicinfo::tppicinfo(){

}
tppicinfo::tppicinfo(QString name,QString radian,QString perimeter,QString height,QString area,QString distance,QString angle,QString width,QString radius){
    m_name=name;
    m_radian=radian;
    m_perimeter=perimeter;
    m_height=height;
    m_area=area;
    m_distance=distance;
    m_angle=angle;
    m_width=width;
    m_radius=radius;
}
tppicinfo::~tppicinfo(){

}


QString tppicinfo::getName() const
{
    return m_name;
}

void tppicinfo::setName(const QString &name)
{
    m_name = name;
}

QString tppicinfo::getRadian() const
{
    return m_radian;
}

void tppicinfo::setRadian(const QString &radian)
{
    m_radian = radian;
}

QString tppicinfo::getPerimeter() const
{
    return m_perimeter;
}

void tppicinfo::setPerimeter(const QString &perimeter)
{
    m_perimeter = perimeter;
}

QString tppicinfo::getHeight() const
{
    return m_height;
}

void tppicinfo::setHeight(const QString &height)
{
    m_height = height;
}

QString tppicinfo::getArea() const
{
    return m_area;
}

void tppicinfo::setArea(const QString &area)
{
    m_area = area;
}

QString tppicinfo::getDistance() const
{
    return m_distance;
}

void tppicinfo::setDistance(const QString &distance)
{
    m_distance = distance;
}

QString tppicinfo::getAngle() const
{
    return m_angle;
}

void tppicinfo::setAngle(const QString &angle)
{
    m_angle = angle;
}

QString tppicinfo::getWidth() const
{
    return m_width;
}

void tppicinfo::setWidth(const QString &width)
{
    m_width = width;
}

QString tppicinfo::getRadius() const
{
    return m_radius;
}

void tppicinfo::setRadius(const QString &radius)
{
    m_radius = radius;
}


