#include <PSmtp.h>


//构造函数记录服务器，端口
PSmtp::PSmtp(QString server,quint16 port,QString sender,QString pwd,QString receiver,QString title,QString content,QString attachment,QObject *parent)
    :QObject(parent)
{
    this->p_server = server;
    this->p_port = port;
    this->p_username = sender;
    this->p_pwd = pwd;
    this->p_receive = receiver;
    this->p_title = title;
    this->p_content = content;
    this->p_attachemnt = attachment;
    this->p_status=0;

    tcpSocket = new QTcpSocket;

    connect(tcpSocket,SIGNAL(connected()),this,SLOT(connectToServer()));
    connect(tcpSocket,SIGNAL(disconnected()),this,SLOT(disconnectFromServer()));
    connect(tcpSocket,SIGNAL(readyRead()),this,SLOT(getMessage()));

    qDebug()<<"PSmtp";

    if(p_title=="")
        p_title=tr("我是汉字");
    if(p_content=="")
        p_content=tr("该邮件用于功能测试");
    qDebug()<<"email send to "+p_receive;
}


//析构 删除tcpSocket
PSmtp::~PSmtp(){
    delete tcpSocket;
    qDebug()<<"~PSmtp";
}

//发送邮件   参数 账号 密码 收件人 主题 正文 附件列表 服务器（qq/163） 是否使用ssl
void PSmtp::sendMail(){
    qDebug()<<"sendMail";

    //连接服务器
    tcpSocket->abort();
    tcpSocket->connectToHost(p_server,p_port);

}




void PSmtp::connectToServer()
{
    qDebug()<<"connect to server";
}



void PSmtp::disconnectFromServer()
{
    qDebug()<<"disconnect from server";
    emit disconnected();
}



void PSmtp::getMessage()
{
    //确定编码格式避免中文乱码
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(codec);
    QTextCodec::setCodecForLocale(QTextCodec::codecForLocale());
    QTextCodec::setCodecForCStrings(QTextCodec::codecForLocale());


    //curText 记录服务器返回数据
    QByteArray curText = tcpSocket->readAll();
    serverText.append(curText);

    //text 将数据封装成STMP邮件格式
    QByteArray text;
//    qDebug()<<curText<<"p_status"<<p_status;

    if(serverText.indexOf("Error")!=-1 || serverText.indexOf("503")!=-1 || serverText.indexOf("421")!=-1 || serverText.indexOf("535")!=-1){
        //535登录验证失败
        qDebug()<<"get error message  ### status= "<<p_status;
        printf("Email send failed!");
        qDebug()<<serverText;
        emit emailStatus(EMAIL_ERROR);
        tcpSocket->disconnectFromHost();
        return;
    }
    if(p_status==5 && serverText.indexOf("250")!=-1)
    {
        //登出
        text.append("quit\r\n");
        p_status =6;
//        qDebug()<<text;
    }
    else if (p_status==4 && serverText.indexOf("354")!=-1)
    {

        //发送信息
        text.append("Subject:");
        text.append("=?utf8?B?"+p_title.toUtf8().toBase64()+"?=");
        qDebug()<<p_title.toUtf8();

        text.append("\r\n");
        text.append("To: ");
        text.append(p_receive);
        text.append("\r\nFrom: ");
        text.append(p_username);
        text.append("\r\n");


        QString t = p_content;

        if(p_attachemnt==""){

            //纯文本发送 无附件
            text.append("Content-Type: text/plain;charset=UTF-8;\r\n");
            text.append("Content-Transfer-Encoding: base64\r\n\r\n");
            t.replace("\n", "\r\n").replace("\r\n.\r\n", "\r\n..\r\n");
            text.append(t.toUtf8().toBase64());
            text.append("\r\n.\r\n");

        }else{

            //带附件发送
            text.append("Content-Type: multipart/mixed;boundary=#BOUNDARY#\r\n");
            text.append("Content-Transfer-Encoding: base64\r\n");
            text.append("MIME-Version: 1.0\r\n\r\n");
            text.append("This is a multi-part message in MIME format.\r\n\r\n");
            text.append("--#BOUNDARY#\r\n");
            text.append("Content-Type:text/plain;charset=UTF-8\r\nContent-Transfer-Encoding:base64\r\n\r\n");
            text.append(t.toUtf8().toBase64());
            text.append("\r\n\r\n");


            //遍历附件列表
            QFile file(p_attachemnt.replace("\n","").replace("\r\n",""));//去掉换行符
            QFileInfo info(file);

            //判断附件类型 封装附件


            if(info.suffix()=="png"){//png文件
                if(file.open(QIODevice::ReadOnly))
                {
                    text.append("--#BOUNDARY#\r\n");
                    text.append("Content-Type:image/png;name="+info.fileName()+"\r\nContent-Transfer-Encoding:base64\r\n");
                    text.append("Content-Disposition: attachment; filename=\""+info.fileName()+"\"\r\n\r\n");
                    text.append(file.readAll().toBase64());
                    text.append("\r\n\r\n");
                    qDebug()<<qPrintable(p_attachemnt)<<"opened successfully!";
                    file.close();
                }else{
                    qDebug()<<qPrintable(p_attachemnt)<<"opened failed";
                }
            }
            else if(info.suffix()=="jpg"||info.suffix()=="jpeg"){//jpg文件 jpeg文件
                if(file.open(QIODevice::ReadOnly))
                {
                    text.append("--#BOUNDARY#\r\n");
                    text.append("Content-Type:image/jpeg;name="+info.fileName()+"\r\nContent-Transfer-Encoding:base64\r\n");
                    text.append("Content-Disposition: attachment; filename=\""+info.fileName()+"\"\r\n\r\n");
                    text.append(file.readAll().toBase64());
                    text.append("\r\n\r\n");
                    qDebug()<<qPrintable(p_attachemnt)<<"opened successfully!";
                    file.close();
                }else{
                    qDebug()<<qPrintable(p_attachemnt)<<"opened failed";
                }
            }
            else if(info.suffix()=="tif"){//tif文件
                if(file.open(QIODevice::ReadOnly))
                {
                    text.append("--#BOUNDARY#\r\n");
                    text.append("Content-Type:image/tiff;name="+info.fileName()+"\r\nContent-Transfer-Encoding:base64\r\n");
                    text.append("Content-Disposition: attachment; filename=\""+info.fileName()+"\"\r\n\r\n");
                    text.append(file.readAll().toBase64());
                    text.append("\r\n\r\n");
                    qDebug()<<qPrintable(p_attachemnt)<<"opened successfully!";
                    file.close();
                }else{
                    qDebug()<<qPrintable(p_attachemnt)<<"opened failed";
                }
            }
            text.append("--#BOUNDARY#--\r\n");
            text.append(".\r\n");
        }

        p_status = 5;
        qDebug()<<qPrintable(text);
    }
    else if (p_status==3 && serverText.indexOf("250")!=-1)
    {
        //DATA
        text.append("DATA\r\n");

        p_status=4;
    }
    else if(p_status==2 && serverText.indexOf("235")!=-1)
    {
        //收件人信息 寄件人信息
        text.append("mail from:<");
        text.append(p_username);
        text.append(">\r\n");
        text.append("rcpt to: <");
        text.append(p_receive);
        text.append(">\r\n");

        p_status = 3;
    }
    else if (p_status==1 && serverText.indexOf("334")!=-1)
    {
        //登录
        text.append(QByteArray().append(p_username).toBase64());
        text.append("\r\n");
        text.append(QByteArray().append(p_pwd).toBase64());
        text.append("\r\n");

        p_status = 2;
    }
    else if (p_status==0 && serverText.indexOf("220")!=-1)
    {
        //打招呼
        text.append("EHLO ");
        text.append(p_server);
        text.append("\r\nAUTH LOGIN\r\n");

        p_status = 1;
    }
    tcpSocket->write(text);
   // qDebug()<<"text:"<<text;

    emit progress((double) p_status/6);
    if(p_status == 6)
    {
        qDebug()<<"E-mail sent successfully!";
        emit emailStatus(EMAIL_SUCCEED);
        tcpSocket->disconnectFromHost();
    }
}

