#include "Ethread.h"

#include <QProcess>
#include <QDebug>

Ethread::Ethread(QString server,QThread *parent)
    :QThread(parent),
      p_server(server)
{

}
Ethread::~Ethread(){

}
void Ethread::run(){
    QProcess *pingprocess = new QProcess;

    pingprocess->start(QString("ping -n 10 "+p_server),QIODevice::ReadOnly);
    pingprocess->waitForFinished(-1);
    QString repingStr = QString::fromLocal8Bit(pingprocess->readAllStandardOutput());
    if(repingStr.contains("TTL")){
        qDebug()<<true;
       emit isOnline(true);
    }else{
        qDebug()<<false;
        emit isOnline(false);
    }
}
