#ifndef PSMTP_H
#define PSMTP_H

#include<QObject>
#include<QTcpSocket>
#include<QDebug>
#include<QStringList>
#include<QFile>
#include<QFileInfo>
#include<QImage>
#include<QTextCodec>
#define EMAIL_ERROR     0   //邮件发送失败
#define EMAIL_SUCCEED   1   //邮件发送成功
#define STATUS_MAX 6.0



//PSmtp 用于发送邮件
class PSmtp : public QObject{
    Q_OBJECT
public:
    PSmtp(QString server,quint16 port,QString username,QString pwd,QString receive,QString title,QString content,QString attachment,QObject *parent=NULL);
    ~PSmtp();

    void sendMail();

private:
    QString p_username;  //用户名 例：pepsi_juice@163.com
    QString p_pwd;       //授权码
    QString p_receive;   //收件人
    QString p_title;     //标题
    QString p_content;   //正文
    QString p_server;    //服务器 smtp.qq.com/smtp.163.com
    quint16 p_port;      //端口 默认25
    QString p_attachemnt;    //附件列表
    int p_status;
    QTcpSocket *tcpSocket;
    QByteArray serverText;


signals:
    void disconnected();
    void emailStatus(int status);
    void progress(double p);

public slots:
    void connectToServer();
    void disconnectFromServer();
    void getMessage();



};



#endif // PSMTP_H
