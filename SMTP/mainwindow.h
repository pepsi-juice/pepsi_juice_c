#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLayout>
#include <QLabel>
#include <QLineEdit>
#include <QString>
#include <QTextCodec>
#include <QPushButton>
#include <QTextEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <QThread>
#include <QMimeData>
#include <mimetic/mimetic.h>
#include <QTcpSocket>
#include <QDialog>
#include <QThread>
#include <QMutex>
#include <QTimer>
#include <QNetworkConfigurationManager>
#include <QProcess>
#include <QSettings>

#include <PSmtp.h>
//MainWindow类
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = NULL);
    ~MainWindow();
    QWidget *mainwidget;
    QMessageBox mb;

    QWidget *receiverWidget;
    QWidget *senderWidget;
private:
    QSettings *sendsettings;
    QSettings *receivesettings;
    QSettings *lastsettings;
    QComboBox  *sendEdit;
    QComboBox  *receiveEdit;

public slots:
    void chooseAttachment();
    void send();
    void sendStatus(int status);
    void checkonline(bool);
    void sendresult(int);
    void checkActive();
    void deleteSender();
    void showPwdTips();
    void deleteReceiver();

signals:
    void isonline(bool);
};




#endif // MAINWINDOW_H
