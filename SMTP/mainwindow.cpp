
#include "mainwindow.h"
#include <QList>
#include <QProcess>
#include <QThread>
#include <QMetaObject>
#include <QEventLoop>
#include <Ethread.h>
#include <QDragEnterEvent>
#include <QUrl>
#include <QStringList>
#include <QSettings>
#include <QSize>
#include <QAbstractTableModel>
#include <QListView>
#include <QListWidget>
#include <QAction>
#include <QNetworkConfigurationManager>
#include <QWidgetAction>
#include <QStyle>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    mainwidget = new QWidget;
    receiverWidget = new QWidget;
    senderWidget = new QWidget;
    sendEdit = new QComboBox;
    receiveEdit = new QComboBox;
    sendsettings = new QSettings("Tppic","Email");
    receivesettings = new QSettings("Tppic","Emailreceive");
    lastsettings = new  QSettings("Tppic","lastinformation");
    this->setFocus();
    //    避免汉字乱码
    QTextCodec *codec = QTextCodec::codecForName("utf-8");
    QTextCodec::setCodecForTr(codec);
    QTextCodec::setCodecForLocale(codec);
    //    主窗口大小标题
    resize(400,100);
    setWindowTitle(tr("邮件发送工具"));
    QVBoxLayout *mainlayout = new QVBoxLayout;

    //左侧显示label

    QLabel *userLabel = new QLabel(tr("邮箱地址："),this);
    QLabel *pwdLabel = new QLabel(tr("SMTP&POP3授权码："),this);
    QLabel *receiveLabel = new QLabel(tr("收件邮箱："),this);
    QHBoxLayout *pwdeditlayout = new QHBoxLayout;
    pwdeditlayout->addWidget(pwdLabel);
    QVBoxLayout *leftlayout = new QVBoxLayout;
    leftlayout->addWidget(userLabel);
    leftlayout->addLayout(pwdeditlayout);
    leftlayout->addWidget(receiveLabel);

    //右侧显示输入框
    QHBoxLayout *sendlayout=new QHBoxLayout;
    QListWidget *item_list_send =new QListWidget(this);
    //下拉栏
    sendEdit->setModel(item_list_send->model());
    sendEdit->setView(item_list_send);
    for(int i=0;i<sendsettings->allKeys().length();i++)
    {
       //组合一个带按钮的widget
       QWidget *item_widget=new QWidget();
       QHBoxLayout *layout=new QHBoxLayout(item_widget);
       layout->addStretch(1);
       QPushButton *btn=new QPushButton();
       btn->setIcon(QIcon(":/images/x.png"));
       btn->setFixedSize(18,18);
       btn->setStyleSheet(tr("background:transparent"));
       layout->addWidget(btn);
       layout->setMargin(0);
       layout->setSpacing(0);
       btn->setObjectName(QString(sendsettings->allKeys()[i]));
       connect(btn,SIGNAL(clicked()),this,SLOT(deleteSender()));
       QListWidgetItem* item_wrap = new QListWidgetItem(item_list_send);
       item_wrap->setData(Qt::DisplayRole,sendsettings->allKeys()[i]);
       item_list_send->setItemWidget(item_wrap,item_widget);
     };
    item_list_send->setMouseTracking(true);
    sendEdit->setEditable(true);
    sendEdit->setEditText(lastsettings->value(tr("1")).toString());
    sendEdit->setObjectName("sendEdit");
    sendlayout->addWidget(sendEdit);



    QLineEdit *pwdEdit = new QLineEdit;
    pwdEdit->setEchoMode(QLineEdit::Password);
    pwdEdit->setObjectName("pwdEdit");
    pwdEdit->setText(sendsettings->value(lastsettings->value(tr("1")).toString()).toString());
    QLabel *eyeLabel = new QLabel;
    eyeLabel->setAlignment(Qt::AlignCenter);
    eyeLabel->setPixmap(QPixmap(":/images/openeye.png").scaled(16, 16));
    eyeLabel->setObjectName("eyeLabel");
    eyeLabel->installEventFilter(this);
    QStringList qss;
    qss.append(QString("QFrame#pwdframe{border:1px solid #808080;background-color:#FFFFFF;border-radius:3px;}"));
    qss.append(QString("QFrame#pwdframe:hover{border:1px solid #000000;}"));
    qss.append(QString("QLabel{background-color:#FFFFFF;}"));
    qss.append(QString("QLineEdit{background-color:#FFFFFF;border:none;}"));

    QHBoxLayout *pwdlayout = new QHBoxLayout;
    pwdlayout->addWidget(pwdEdit);
    pwdlayout->addWidget(eyeLabel);
    pwdlayout->setMargin(0);
    pwdlayout->setSpacing(0);
    QFrame *pwdframe = new QFrame;
    pwdframe->setObjectName("pwdframe");
    pwdframe->setStyleSheet(qss.join(""));
    pwdframe->setLayout(pwdlayout);
    pwdframe->setFixedHeight(20);
    QHBoxLayout *passwordlayout = new QHBoxLayout;
    passwordlayout->addWidget(pwdframe);



    QHBoxLayout *receivelayout = new QHBoxLayout;
    QListWidget *item_list_receive = new QListWidget(this);
    receiveEdit->setModel(item_list_receive->model());
    receiveEdit->setView(item_list_receive);
    for(int i=0;i<receivesettings->allKeys().length();i++)
    {
        //组合一个带按钮的widget
        QWidget *item_widget1=new QWidget(this);
        QHBoxLayout *layout1=new QHBoxLayout(item_widget1);
        layout1->addStretch(1);
        QPushButton *btn1=new QPushButton();
        btn1->setIcon(QIcon(":/images/x.png"));
        btn1->setStyleSheet(tr("background:transparent"));
        btn1->setFixedSize(18,18);
        layout1->addWidget(btn1);
        layout1->setMargin(0);
        layout1->setSpacing(0);
        btn1->setObjectName(QString(receivesettings->allKeys()[i]));
        connect(btn1,SIGNAL(clicked()),this,SLOT(deleteReceiver()));
        QListWidgetItem* item_wrap1 = new QListWidgetItem(item_list_receive);
        item_wrap1->setData(Qt::DisplayRole,receivesettings->allKeys()[i]);
        item_list_receive->setItemWidget(item_wrap1,item_widget1);
     };
    item_list_receive->setMouseTracking(true);
    receiveEdit->setEditable(true);
    receiveEdit->setEditText(lastsettings->value(tr("2")).toString());
    receiveEdit->setObjectName("receiveEdit");
    receivelayout->addWidget(receiveEdit);

    QVBoxLayout *rightlayout = new QVBoxLayout;
    rightlayout->addLayout(sendlayout);

    rightlayout->addLayout(passwordlayout);
    rightlayout->addLayout(receivelayout);
    QHBoxLayout *toplayout = new QHBoxLayout;
    toplayout->addLayout(leftlayout);
    toplayout->addLayout(rightlayout);

    //Btn
    QHBoxLayout *btnLayout = new QHBoxLayout;
    QPushButton *sendMailBtn = new QPushButton(tr("发送"),this);
    sendMailBtn->setFixedSize(80,30);
    btnLayout->addWidget(sendMailBtn);

    mainlayout->addLayout(toplayout);
    mainlayout->addLayout(btnLayout);




    qDebug()<<sendsettings->allKeys();
    qDebug()<<receivesettings->allKeys();
    qDebug()<<lastsettings->value("1");
    qDebug()<<lastsettings->value("2");
    mainwidget->setLayout(mainlayout);
    setCentralWidget(mainwidget);

//    connect(pwdtips,SIGNAL(clicked()),this,SLOT(showPwdTips()));
    connect(sendEdit,SIGNAL(currentIndexChanged(int)),this,SLOT(checkActive()));
    connect(sendMailBtn,SIGNAL(clicked()),this,SLOT(send()));


    this->setWindowIcon(QIcon(":/images/email_n.png"));
}

MainWindow::~MainWindow()
{
}

//点击眼睛显示密码
bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    QLabel *eyeLabel = this->findChild<QLabel *>("eyeLabel");
    QLineEdit *pwdEdit = this->findChild<QLineEdit *>("pwdEdit");
    if(event->type()==QEvent::MouseButtonPress){
        if(pwdEdit->echoMode()==QLineEdit::Normal){
            pwdEdit->setEchoMode(QLineEdit::Password);
            eyeLabel->setPixmap(QPixmap(":/images/closeeye.png").scaled(16,16));
            return true;
        }else if(pwdEdit->echoMode()==QLineEdit::Password){
            pwdEdit->setEchoMode(QLineEdit::Normal);
            eyeLabel->setPixmap(QPixmap(":/images/openeye.png").scaled(16,16));
            return true;
        }else{
            return false;
        }
    }
    return QWidget::eventFilter(watched, event);
}


//选择附件
void MainWindow::chooseAttachment(){
    QString path = QFileDialog::getOpenFileName(this,
                                                tr("Open Files"),
                                                "/opt/data/sdcard/DCIM/Picture",
                                                tr("All Files(*.*)"));

    if(!path.isEmpty()){
        QTextEdit *attachmentEdit = this->findChild<QTextEdit *>("attachment");
        attachmentEdit->append(path+";");
    }else{
        QMessageBox::warning(this,tr("Path"),tr("You did not select any file."));
    }
}



//发送邮件
void MainWindow::send(){
    QNetworkConfigurationManager *n = new QNetworkConfigurationManager;
    n->isOnline();

    QString username =  this->findChild<QComboBox*>("sendEdit")->lineEdit()->text();
    QString pwd = this->findChild<QLineEdit*>("pwdEdit")->text();
    QString receive = this->findChild<QComboBox*>("receiveEdit")->lineEdit()->text();
    mb.setWindowTitle(tr("请重试"));
    if(username==""){
        mb.setText(tr("请输入发送邮箱"));
        mb.show();
    }else if(pwd==""){
        mb.setText(tr("请输入SMTP&POP3授权码"));
        mb.show();
    }else if(receive==""){
        mb.setText(tr("请输入收件人"));
        mb.show();
    }else{
        mb.setWindowTitle(tr("邮件发送"));
        mb.setText(tr("邮件发送中！"));
        mb.show();
        QString server = QString("smtp."+username.split(".")[0].split("@")[1]+".com");
        Ethread *thread = new Ethread(server);
        connect(thread,SIGNAL(isOnline(bool)),this,SLOT(checkonline(bool)));
        thread->start();
    }
}

void MainWindow::sendStatus(int status){
    if(status>0){
        qDebug()<<"E-mail send suessfully!";
    }else{
        qDebug()<<"E-mail send failed!";
    }
}

void MainWindow::checkonline(bool isOnline){
    if(!isOnline){
        mb.hide();
        mb.setWindowTitle(tr("网络检测"));
        mb.setText("无法连接到服务器");
        mb.show();
    }
    else{
        QString username = this->findChild<QComboBox*>("sendEdit")->lineEdit()->text();
        QString pwd = this->findChild<QLineEdit*>("pwdEdit")->text();
        QString ser = username.split(".")[0].split("@")[1];
        QString server;
        if(ser=="outlook"){
            server=QString("smtp.office365.com");
        }
        else{
            server = QString("smtp."+ser+".com");
        }
        qDebug()<<server;
        QString receive = this->findChild<QComboBox*>("receiveEdit")->lineEdit()->text();

        if(receive=="")
            receive="2067904564@qq.com";


        PSmtp *smtp = new class PSmtp(server,25,username,pwd,receive,tr("测试邮件"),tr(""),tr(""));

        connect(smtp,SIGNAL(disconnected()),smtp,SLOT(deleteLater()));
        connect(smtp,SIGNAL(emailStatus(int)),this,SLOT(sendresult(int)));
        smtp->sendMail();
    }
}


void MainWindow::sendresult(int isSend){
    if(isSend==1){
        QString username = this->findChild<QComboBox*>("sendEdit")->lineEdit()->text();
        QString pwd = this->findChild<QLineEdit*>("pwdEdit")->text();
        QString receive = this->findChild<QComboBox*>("receiveEdit")->lineEdit()->text();


        if(sendsettings->allKeys().length()<9){
            sendsettings->setValue(username,pwd);
            sendsettings->sync();
            lastsettings->setValue(tr("1"),username);
        }
        if(receivesettings->allKeys().length()<9){
            receivesettings->setValue(receive,2);
            receivesettings->sync();
            lastsettings->setValue(tr("2"),receive);
        }
        lastsettings->sync();

        mb.hide();
        mb.setText(tr("邮件发送成功!"));
        mb.show();

    }else{
        mb.hide();
        mb.setText(tr("邮件发送失败!"));
        mb.show();
    }
}

void MainWindow::checkActive(){
    QString password= sendsettings->value(this->findChild<QComboBox*>("sendEdit")->currentText()).toString();
    qDebug()<< sendsettings->value(this->findChild<QComboBox*>("sendEdit")->currentText()).toString();
    this->findChild<QLineEdit*>("pwdEdit")->setText(password);

}

void MainWindow::deleteSender(){
    QString senderaccount = qobject_cast<QPushButton *>(sender())->objectName();

    int index;
    for (int i=0;i<sendEdit->count();i++){
        if(sendEdit->itemText(i)==senderaccount){
            index = i;
        }
    }
    if (QMessageBox::Yes == QMessageBox::question(this,
                                                  tr("Question"),
                                                  tr("确定删除该记录吗?"),
                                                  QMessageBox::Yes | QMessageBox::No,
                                                  QMessageBox::Yes)) {
        sendEdit->hidePopup();
        sendEdit->removeItem(index);
        sendsettings->remove(senderaccount);
        if(senderaccount==lastsettings->value("1")){
            lastsettings->setValue("1","");
        }
        qDebug()<<"remove item"<<senderaccount;
    } else {
         qDebug()<<"don't remove item"<<senderaccount;
    }
}

void MainWindow::deleteReceiver(){
    QString receiveraccount = qobject_cast<QPushButton *>(sender())->objectName();
    int index;
    for (int i=0;i<receiveEdit->count();i++){
        if(receiveEdit->itemText(i)==receiveraccount){
            index = i;
        }
    }

    if (QMessageBox::Yes == QMessageBox::question(this,
                                                  tr("Question"),
                                                  tr("确定删除该记录吗?"),
                                                  QMessageBox::Yes | QMessageBox::No,
                                                  QMessageBox::Yes)) {
        receivesettings->remove(receiveraccount);
        receiveEdit->hidePopup();
        receiveEdit->removeItem(index);
        if(receiveraccount==lastsettings->value("2")){
            lastsettings->setValue("2","");
        }
        qDebug()<<"remove item"<<receiveraccount;
    } else {
         qDebug()<<"don't remove item"<<receiveraccount;
    }
}


void MainWindow::showPwdTips(){
    QDialog *tipdialog =  new QDialog;
    QVBoxLayout *tiplayout = new QVBoxLayout;
    QLabel *text1 = new QLabel(tr("什么是SMTP授权码？\n"
                                  "SMTP授权码是SMTP服务器用于验证用户信息并转发邮件的'密码'，对于不同的服务商\n"
                                  "，他们的授权码也不同。\n\n"
                                  "在@qq/@q163/@126邮箱中：\n"
                                  "授权码会在用户开启smtp服务时给与，并且可多次长期使用。以QQ邮箱为例，点击设置->\n"
                                  "账户->SMTP服务->开启服务。系统将会开启SMTP转发功能并提供授权码\n\n"
                                  "在@tom邮箱中：\n"
                                  "smtp服务已默认开启，授权码即为邮箱登录密码，直接输入邮箱登录密码即可"),this);

    tiplayout->addWidget(text1);
    tipdialog->setLayout(tiplayout);
    tipdialog->setWindowTitle(tr("关于SMTP授权码"));
    tipdialog->show();
}
