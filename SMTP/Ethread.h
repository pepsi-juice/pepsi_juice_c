#ifndef ETHREAD_H
#define ETHREAD_H
#include <QObject>
#include <QThread>

class Ethread : public QThread
{
    Q_OBJECT
public:
    Ethread(QString server,QThread *parent = NULL);
    ~Ethread();
protected:
    void run();
private:
    QString p_server;
signals:
    void isOnline(bool);
};


#endif // ETHREAD_H
