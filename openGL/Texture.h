#ifndef TEXTURE_H
#define TEXTURE_H

#include "glad\glad.h"
#include "GLFW\glfw3.h"
#include "stb_image.h"

#include <iostream>
#include <string>



class Texture
{
public:
	Texture(const char* path);
	

	void texParameteri(int des, int axis, int method);
	void active(int unit);
	void generateMipmaps(int target);

private:
	unsigned int texture;
	unsigned char* data;
	int height;
	int width;
	int nrChannel;
};

#endif