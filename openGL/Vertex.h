#pragma once
#include "glad\glad.h"
#include "GLFW\glfw3.h"
#include "glm\glm\glm.hpp"
#include "glm\glm\gtc\matrix_transform.hpp"
#include "glm\glm\gtc\type_ptr.hpp"

#include <iostream>

class Vertex
{
public:

	Vertex(float vertices[],int size);
	void draw();

private:
	unsigned int VBO;
	unsigned int VAO;
};

