#include "Texture.h"
Texture::Texture(const char* path = nullptr)
{
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	data = stbi_load(path, &width, &height, &nrChannel, 4);
}



//设置参数
void Texture::texParameteri(int des, int axis, int method)
{
	glBindTexture(GL_TEXTURE_2D,texture);
	glTexParameteri(des, axis, method);

}

//激活纹理单元
void Texture::active(int unit)
{
	glActiveTexture(unit);
	glBindTexture(GL_TEXTURE_2D, texture);
}

void Texture::generateMipmaps(int target)
{
	glBindTexture(GL_TEXTURE_2D, texture);
	stbi_set_flip_vertically_on_load(true);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);//自动生成多级渐远纹理
	stbi_image_free(data);
}